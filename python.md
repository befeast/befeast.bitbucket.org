## Python interview questions

- PYTHON OBJECTS: MUTABLE VS. IMMUTABLE
[https://codehabitude.com/2013/12/24/python-objects-mutable-vs-immutable/]()

- Passing objects to funtion 
[http://robertheaton.com/2014/02/09/pythons-pass-by-object-reference-as-explained-by-philip-k-dick/]()

- Iterators/coroutines 
[http://wla.berkeley.edu/~cs61a/fa11/lectures/streams.html]()